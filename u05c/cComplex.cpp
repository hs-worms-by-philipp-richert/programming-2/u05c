#include "cComplex.h"

// Konstruktor
cComplex::cComplex(double re_in, double im_in) {
	re = re_in;
	im = im_in;
}

// Abstand der komplexen Zahl zum Nullpunkt
double cComplex::distanceNull() {
	double d = sqrt(pow(re, 2) + pow(im, 2));
	return d;
}

// Textausgabe der Objektwerte
void cComplex::printme() {
	cout << "c = " << re << "+" << im << "i mit Abstand zu Null d = " << cComplex::distanceNull() << endl;
}

// Komplexe Addition
cComplex add(cComplex c1, cComplex c2) {
	double re_sum = c1.re + c2.re;
	double im_sum = c1.im + c2.im;
	return cComplex(re_sum, im_sum);
}

// Komplexe Subtraktion
cComplex subt(cComplex c1, cComplex c2) {
	double re_subt = c1.re - c2.re;
	double im_subt = c1.im - c2.im;
	return cComplex(re_subt, im_subt);
}

// Komplexe Multiplikation
cComplex mul(cComplex c1, cComplex c2) {
	double re_prod = c1.re * c2.re;
	double im_prod = c1.im * c2.im;
	return cComplex(re_prod, im_prod);
}

// Komplexe Division
cComplex div(cComplex c1, cComplex c2) {
	if (c2.re == 0.0 || c2.im == 0.0) {	// Nulldivison abfangen
		cout << "Nulldivision ist nicht erlaubt!" << endl;
		return cComplex();
	}
	double re_div = c1.re / c2.re;
	double im_div = c1.im / c2.im;
	return cComplex(re_div, im_div);
}

// Distanz zum Nullpunkt zw. c1 und c2 vergleichen
int complCompare(cComplex c1, cComplex c2) {
	double d1 = c1.distanceNull();
	double d2 = c2.distanceNull();

	if (d1 > d2) return 1;
	else if (d1 == d2) return 0;
	else if (d1 < d2) return -1;
}

// Getter fuer Realteil
double cComplex::getRe() {
	return re;
}

// Getter fuer Imaginaerteil
double cComplex::getIm() {
	return im;
}