#pragma once

#include <cmath>
#include <iostream>

using namespace std;

class cComplex
{
private:
	double re;
	double im;
	friend cComplex add(cComplex, cComplex);
	friend cComplex subt(cComplex, cComplex);
	friend cComplex mul(cComplex, cComplex);
	friend cComplex div(cComplex, cComplex);
	friend int complCompare(cComplex, cComplex);
public:
	cComplex(double = 0.0, double = 0.0);
	double distanceNull();
	void printme();
	double getRe();
	double getIm();
};
