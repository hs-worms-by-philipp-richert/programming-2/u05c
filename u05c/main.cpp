// Aufgabe u05c
// Mathematische Operationen mit imaginaeren Zahlen
// Philipp Richert
// 15.11.2022

#include "cComplex.h"

int main() {
	cComplex a = { 1, 2 };
	cComplex b = { 2, 2 };

	cComplex c = add(a, b);
	cComplex d = subt(a, b);
	cComplex e = mul(a, b);
	cComplex f = div(a, b);

	a.printme();
	b.printme();
	c.printme();
	d.printme();
	e.printme();
	f.printme();

	cout << endl << "Distanz zu Null von a und b vergleichen: " << complCompare(a, b) << endl;

	return 0;
}